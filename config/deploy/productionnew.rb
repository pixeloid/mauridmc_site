set :stage, :productionnew
set :wpcli_remote_url, 'http://mauridmc.com'
set :tmp_dir, '/home/mauri/mauridmc.com/tmp'


set :deploy_to, -> { "/home/mauri/mauridmc.com/mauridmc.hu/" }
set :user, "mauri"
# wVz!X}F!Sk&-

# Simple Role Syntax
# ==================
role :app, %w{mauri@mauri.hu}
role :web, %w{mauri@mauri.hu}
role :db,  %w{mauri@mauri.hu}

# Extended Server Syntax
# ======================
server 'mauri.hu', user: 'mauri', roles: %w{web app db}

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
  port: '8822'
 }

set :wpcli_rsync_port, 8822


fetch(:default_env).merge!(wp_env: :productionnew)

