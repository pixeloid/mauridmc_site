set :application, 'mauridmc'
set :repo_url, 'https://pixeloid@bitbucket.org/pixeloid/mauridmc_site.git'
set :composer_install_flags, '--no-dev --no-interaction --quiet --optimize-autoloader --ignore-platform-reqs'
set :rsync_options, %w[--recursive --delete --delete-excluded --exclude .git*]

# Branch options
# Prompts for the branch name (defaults to current branch)
#ask :branch, -> { `git rev-parse --abbrev-ref HEAD`.chomp }

# Hardcodes branch to always be master
# This could be overridden in a stage config file
set :branch, :master


set :wpcli_local_url, "http://localhost:8000"


# Use :debug for more verbose output when troubleshooting
set :log_level, :info

# Apache users with .htaccess files:
# it needs to be added to linked_files so it persists across deploys:
# set :linked_files, fetch(:linked_files, []).push('.env', 'web/.htaccess')
set :linked_files, fetch(:linked_files, []).push('.env', 'web/.htaccess')
set :linked_dirs, fetch(:linked_dirs, []).push('web/app/uploads')

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :service, :nginx, :reload
    end
  end
end

# The above restart task is not run by default
# Uncomment the following line to run it on deploys if needed
# after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  desc 'Update WordPress template root paths to point to the new release'
  task :update_option_paths do
    on roles(:app) do
      within fetch(:release_path) do
        if test :wp, :core, 'is-installed'
          [:stylesheet_root, :template_root].each do |option|
            # Only change the value if it's an absolute path
            # i.e. The relative path "/themes" must remain unchanged
            # Also, the option might not be set, in which case we leave it like that
            value = capture :wp, :option, :get, option, raise_on_non_zero_exit: false
            if value != '' && value != '/themes'
              execute :wp, :option, :set, option, fetch(:release_path).join('web/wp/wp-content/themes')
            end
          end
        end
      end
    end
  end
end


# namespace :deploy do
#   desc "Push local changes to Git repository"
#   task :push do

#     # Check for any local changes that haven't been committed
#     # Use 'cap deploy:push IGNORE_DEPLOY_RB=1' to ignore changes to this file (for testing)
#     status = %x(git status --porcelain).chomp
#     if status != ""
#       if status !~ %r{^[M ][M ] config/deploy.rb$}
#         raise Capistrano::Error, "Local git repository has uncommitted changes"
#       elsif !ENV["IGNORE_DEPLOY_RB"]
#         # This is used for testing changes to this script without committing them first
#         raise Capistrano::Error, "Local git repository has uncommitted changes (set IGNORE_DEPLOY_RB=1 to ignore changes to deploy.rb)"
#       end
#     end

#     # Check we are on the master branch, so we can't forget to merge before deploying
#     branch = %x(git branch --no-color 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \\(.*\\)/\\1/').chomp
#     if branch != "master" && !ENV["IGNORE_BRANCH"]
#       raise Capistrano::Error, "Not on master branch (set IGNORE_BRANCH=1 to ignore)"
#     end

#     # Push the changes
#     if ! system "git push #{fetch(:repository)} master"
#       raise Capistrano::Error, "Failed to push changes to #{fetch(:repository)}"
#     end

#   end
# end

# The above update_option_paths task is not run by default
# Note that you need to have WP-CLI installed on your server
# Uncomment the following line to run it on deploys if needed
after 'deploy:publishing', 'deploy:update_option_paths'
# before "deploy", "deploy:push"

