//= require "_modernizr.custom.27778"
//= require "jquery"
//= require "bootstrap"
//= require "_jquery.easing.1.3"
//= require "_jquery.parallax-scroll"
//= require "responsive-toolkit"
//= require "wow"
//= require "lightgallery/dist/js/lightgallery-all.js"



+function ($, viewport) {

	var s,
	    APP = {

	        settings : {
	        },

	        init: function () {
	            s = this.settings;
	           	var _this = this;

	           	$(window).bind('load resize', function(){
	           		    _this.alignIllustrations();	           		
	           		    _this.alignReferences();	           		
	           		    _this.alignParallaxImages();
	           	})

	           $(window).bind('scroll', function(){
	           	    if($(window).scrollTop() > 80){
	           	    	$('body').addClass('scrolled')
	           	    }else{
	           	    	$('body').removeClass('scrolled')
	           	    }	           		
	           })

	           $('.hover, .link').bind('touchstart touchend', function(e) {
	               $(this).toggleClass('hover_effect');
	           });

	           _this.initLightbox();	           		

	           
	        },

	        alignIllustrations: function(){

	        	var illHeight = $('.header-illustration img').first().height() 
	        	var textHeight = $('.header-illustration .text').first().height() 

	        	if( viewport.is('>=sm') ) {
	        		$('.header-illustration').height(illHeight);
	        		$('.header-illustration-home .illustration').height($('body').height()-100)
	        		$('.header-illustration .text').css('margin-top', (illHeight - textHeight )/ 2)
	        	}else{
	        		$('.header-illustration .illustration').height(illHeight);
	        	}


	        },

	        alignReferences: function(){


	        	$('.references-item').each(function(){
	        		var carHeight = $('.carousel', this).first().outerHeight() 
	        		var headingHeight = $('.panel-heading', this).first().outerHeight() 

	        		$('.panel', this).height(carHeight + headingHeight)
	        		$('.panel-body', this).height(carHeight)
	        	})

	        },

	        alignParallaxImages: function(){


	        	$('.parallax').each(function(){
	        		var imgHeight = $('img', this).height();

	        		//$(this).height(imgHeight * 0.7)

	        	})

	        },


	        initLightbox: function(){


	        	$(".lightbox").lightGallery({
	        		'selector': '.parallax a',
	        		'thumbnail': false,
	        		'pager': true,
	        		'mode' :'lg-fade'
	        	}); 
	        	$(".carousel").lightGallery({
	        		'selector': '.enlarge',
	        		'thumbnail': false,
	        		'pager': true,
	        		'mode' :'lg-fade'
	        	}); 

	        	$('.zoom').click(function(){
	        		$(this).closest('.carousel').find('.item.active a').click();
	        	})
	        },



	    };

	$(function () {
	    APP.init();

	    $('.carousel').carousel();


	    new WOW().init();

	    $(window).bind('load resize', function(){
	    	$(window).trigger('scroll');   
	    }).trigger('resize')

	});


}(jQuery, ResponsiveBootstrapToolkit);

