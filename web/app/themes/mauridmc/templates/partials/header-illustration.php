	<div class='header-illustration'>
	  <div class='text wow fadeInUp'>
	    <h1>
	      
	      <?php echo types_render_field('illustration-text') ?>

	    </h1>
	  </div>
	  <div class='illustration'>
	  	<?php for ($i=1; $i < 5; $i++): ?> 
	  		<?php $image = types_render_field('illustration-layer-' . $i, array('output'=>'raw')); ?>

	  		<?php if ($image): ?>
	  			<img class="wow <?php echo ( $i % 2 == 0 ) ? 'fadeInLeft' : 'fadeInRight'; ?>" data-wow-delay='0.<?php echo $i*8 ?>s' src='<?php echo $image ?>'>
	  		<?php endif ?>
	  	<?php endfor; ?>
	  </div>
	</div>
