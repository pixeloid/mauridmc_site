<?php
/**
 * Template Name: Latest Trends
 */
?>
<?php wp_reset_query(); ?>

<?php

    $args = array("post_type" => "latest-trend", "order" => "ASC", "orderby" => "menu_order");
    query_posts($args);
?>


<?php get_template_part('templates/partials/header-illustration'); ?>


<?php while (have_posts()) : the_post(); ?>

  <div class='container'>
    <section class='gallery'>
      <h2 class='heading-separated'>
        <span><?php the_title(); ?></span>
      </h2>
      <div class='row'>
        <div class='col-sm-6'>
            <?php 
          echo types_render_field( "image-left", array("class" => "img-responsive", "alt" => "", "size" => "med_gallery" ) )
            ?>

        </div>
        <div class='col-sm-6'>
          <?php 
          echo types_render_field( "image-right", array("class" => "img-responsive", "alt" => "", "size" => "med_gallery" ) )
          ?>
        </div>
      </div>
      <div class='row'>
        <div class='col-md-4 col-md-push-3 col-md-6 col-md-push-3'>
          <p class='title'>
                  <?php echo strip_tags(types_render_field( "latest-trend-title")); ?>
            
            <b>
            <?php echo strip_tags(types_render_field( "latest-trend-description")); ?>
            </b>
          </p>
        </div>
      </div>
    </section>
  </div>


<?php endwhile; wp_reset_query();?>
