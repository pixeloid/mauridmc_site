<?php
/**
 * Template Name: Study Tours
 */
?>

<?php

    $args = array("post_type" => "study-tour-gallery", "order" => "ASC", "orderby" => "menu_order");
    query_posts($args);
?>


<?php get_template_part('templates/partials/header-illustration'); ?>




  <div class='container'>
    <section class='gallery'>
      <h2 class='heading-separated'>
        <span>


          <?php
           $currentlang = get_bloginfo('language');
           if($currentlang=="fr-FR"):
          ?>
galérie           <?php else: ?>
          Gallery



          <?php endif; ?>


        </span>
      </h2>
      <div class='row'>

      <?php while (have_posts()) : the_post(); ?>

        <div class='col-sm-6'>
          <div class="link">
          <?php 
          echo types_render_field( "image", array("class" => "img-responsive", "alt" => "", "width" => "200", "height" => "560", "resize" => "crop" ) )
          ?>
            <div class='text'>
              <h3>
                <?php the_title(); ?>
                <br>
                <?php 
                echo types_render_field( "study-tour-date")
                ?>
              </h3>
              <?php the_content(); ?>
            </div>
          </div>
        </div>

        <?php endwhile; wp_reset_query();?>

      </div>
    </section>
  </div>






