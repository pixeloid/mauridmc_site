<?php 

use Roots\Sage\Nav\BootstrapWalker;

?>

<script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1196542630371212',
      xfbml      : true,
      version    : 'v2.4'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


<div class='navbar navbar-default navbar-fixed-top'>
  <div class='toggle-holder'>
    <button class='navbar-toggle' data-target='.navbar-responsive-collapse' data-toggle='collapse' type='button'>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
      <span class='icon-bar'></span>
      <span class='menutext'>MENU</span>
    </button>
  </div>
  <a class='navbar-brand' href='/'>
   <img alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' class='logo logo-desktop'>
   <img alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' src='<?php echo esc_url( get_theme_mod( 'themeslug_logo_mobile' ) ); ?>' class='logo logo-mobile'>
   <img alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' src='<?php echo esc_url( get_theme_mod( 'themeslug_logo_text' ) ); ?>' class='dmc'>
  </a>

  <div class=' navbar-language-select'>
    <ul style='text-transform: uppercase'>
      <?php pll_the_languages(array('hide_current' => true, 'display_names_as' => 'slug')); ?>
    </ul>
  </div>

  <div class='navbar-collapse navbar-right collapse navbar-responsive-collapse navbar-primary'>
  <?php
  if (has_nav_menu('primary_navigation')) :
    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-main navbar-right', 'walker' => new BootstrapWalker()));
  endif;
  ?>
  </div>
  <?php //dynamic_sidebar('header'); ?>
</div>



