<?php get_template_part('templates/partials/header-illustration'); ?>

<div class='container'>
  <h2 class='heading-separated'>
	  <span><?php the_title(); ?></span>
  </h2>
  <div class='lightbox'>

	<?php while (have_posts()) : the_post(); ?>
	      <?php the_content(); ?>
	<?php endwhile; ?>

	</div>


	<?php
	 $currentlang = get_bloginfo('language');
	 if($currentlang=="fr-FR"):
	?>
<h2 class='heading-separated'>
  <span>Entrons en contact</span>
</h2>
<div class='row'>
  <div class='col-sm-4'>
    <h3>Pour en savoir plus n’hésitez à nous contacter:</h3>
    <p>Nous vous ferons un    plaisir de répondre à votre demande le plus tôt possible !</p>
  </div>
  <div class='col-sm-6 col-sm-push-2'>
    <form action='' class='form-horizontal'>
      <div class='row'>
        <div class='col-sm-6'>
          <input class='form-control' id='firstName' name='firstName' placeholder='Prénom' required='' type='text'>
          <input class='form-control' id='lastName' name='lastName' placeholder='Nom' required='' type='text'>
          <input class='form-control' id='email' name='email' placeholder='E-mail' required='' type='email'>
        </div>
        <div class='col-sm-6'>
          <textarea class='form-control' id='message' name='message' placeholder='Votre message ou demande' required=''></textarea>
        </div>
      </div>
      <div class='row'>
        <div class='col-sm-2 col-sm-push-10'>
          <button class='btn btn-primary btn-block' type='submit'>Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>

	<?php else: ?>




	<h2 class='heading-separated'>
	  <span>Get in touch</span>
	</h2>
	<div class='row'>
	  <div class='col-sm-4'>
	    <h3>Interested in more?</h3>
	    <p>Just fill out this form, give us some questions and we will contact you as soon as possible!</p>
	  </div>
	  <div class='col-sm-6 col-sm-push-2'>
	    <form action='' class='form-horizontal'>
	      <div class='row'>
	        <div class='col-sm-6'>
	          <input class='form-control' id='firstName' name='firstName' placeholder='First Name' required='' type='text'>
	          <input class='form-control' id='lastName' name='lastName' placeholder='Last Name' required='' type='text'>
	          <input class='form-control' id='email' name='email' placeholder='E-mail' required='' type='email'>
	        </div>
	        <div class='col-sm-6'>
	          <textarea class='form-control' id='message' name='message' placeholder='Your message or request' required=''></textarea>
	        </div>
	      </div>
	      <div class='row'>
	        <div class='col-sm-2 col-sm-push-10'>
	          <button class='btn btn-primary btn-block' type='submit'>Submit</button>
	        </div>
	      </div>
	    </form>
	  </div>
	</div>



	<?php endif; ?>


</div>