<?php
/**
 * Template Name: Backstage French
 */
?>

<?php get_template_part('templates/partials/header-illustration'); ?>
<div class="page-backstage">
  <div class="container">
    


      <section class='row steps'>

      <h2 class='heading-separated'>
        <span>
          VOICI LES PRÉPARATIFS À SUIVRE
        </span>
      </h2>

        <div class='col-sm-4 text-center'>
          <h3>
            <b>1er</b>
            rendez-vous
          </h3>
<p>          DÉCOUVRIR les DIFFÉRENTES COLLECTIONS (essayage)</p>
<p>          RÉALISATION de vos croquis en fonction de votre ÉVÉNEMENT, de vos GOÛTS, de votre morphologie.</p>
<p>          ET DES TISSUS qui vou  subliment.</p>
<p>          PRÉSENTATION DU DEVIS.</p>
<p>          Prise de mesure</p>

        </div>
        <div class='col-sm-4 text-center'>
          <h3>
            <b>2ème</b>
            rendez-vous
          </h3>
          <p>ESSAYAGE de votre modèle</p>
          <p>RÉGLAGES, épinglages, DÉCOUPE des décolletés…</p>
        </div>
        <div class='col-sm-4 text-center'>
          <h3>
            <b>3ème</b>
            rendez-vous
          </h3>
          <p>CRÉATION TERMINÉE (et finitions parfaites)</p>
          <p>Voici VOTRE ÉVÉNEMENT UNIQUE  juste pour vous</p>
        </div>
      </section>
      <?php while (have_posts()) : the_post(); ?>

      <?php the_content(); ?>

      <?php endwhile; wp_reset_query();?>

      <section class='general-information'>
        <h2 class='heading-separated'>
          <span>
            information générale
          </span>
        </h2>
        <div class='row'>
          <div class='col-sm-4'>
            <b>Mauri DMC</b>
            <br>
            H-1012 Budapest,
            <br>
            Márvány utca 18.
          </div>
          <div class='col-sm-4'>
            Tel: + 36 1 225 03 60
            <br>
            Fax:+ 36 1 225 03 61
            <br>
            E-mail: incoming@mauridmc.hu
          </div>
          <div class='col-sm-4'>
            www.mauridmc.hu
            <br>
              <a href='https://www.facebook.com/Mauri-DMC-417773631609673/'>
                <i class='fa fa-facebook'></i> Facebook
              </a>
            <br>
              <a href='https://hu.linkedin.com/in/mauri-dmc-dóra-farkas-5780baa3'>
                <i class='fa fa-linkedin'></i> LinkedIn
              </a>
          </div>
        </div>
      </section>
    </div>
    <section class='map'>
      <iframe frameborder='3' height='280' marginheight='0' marginwidth='0' scrolling='no' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2695.7114819496846!2d19.027033200000005!3d47.495533699999974!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741dc25d5af33cf%3A0x51eaca483b2002c8!2sMauri+DMC!5e0!3m2!1sen!2shu!4v1441666534882' width='100%'></iframe>
    </section>
  </div>
</div>
