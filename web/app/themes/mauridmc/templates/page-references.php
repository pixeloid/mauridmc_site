<?php
/**
 * Template Name: References
 */
?>
<?php wp_reset_query(); ?>

<?php

    $args = array("post_type" => "reference", "order" => "ASC", "orderby" => "menu_order");
    query_posts($args);
?>





<div class='container'>

  <div class='references'>
  
    <?php while (have_posts()) : the_post(); ?>
    <div class='references-item'>
      <div class='panel'>
        <div class='panel-heading'>
          <div class='panel-heading-inner'>
            <div class='panel-heading-title'>
              <?php the_title(); ?>
            </div>
            <div class='panel-heading-date'>
              <?php echo types_render_field( "date"); ?>
            </div>
          </div>
        </div>
        <div class='panel-body'>
          <p><?php echo types_render_field( "description"); ?></p>
          <div class='logo text-center'>
            <?php echo types_render_field( "logo"); ?>
          </div>
          <div class='btn-slideup hover'>
            <span>
              <i class='glyphicon glyphicon-chevron-up'></i>
            </span>
          </div>
        </div>
        <?php the_content(); ?>
      </div>
    </div>
    <?php endwhile; wp_reset_query();?>
  </div>
</div>


