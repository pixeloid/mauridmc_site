</div>

<footer class='site-footer'>
  <div class='container'>
    <div class='row'>
      <div class='col-sm-4'>
      <img alt='' class='logo' src='<?php echo esc_url( get_theme_mod( 'themeslug_logo_footer' ) ); ?>'>
        <p class='copy'>
          &copy;2015  Mauri DMC – All rights reserved
        </p>
      </div>
      <div class='col-sm-4 text-center contact'>
        <p>
          <i class='fa fa-home'></i>
          H-1012 Budapest, Márvány utca 18.
          <ul class='list-inline'>
            <li>
              <a href='tel:+3612250360'>
                <i class='fa fa-phone'></i>
                + 36 1 225 03 60
              </a>
            </li>
            <li>
              <a href='tel:+36304666816 '>
                <i class='fa fa-mobile'></i>
                + 36 30 4666 816   
              </a>
            </li>
          </ul>
        </p>
      </div>
      <div class='col-sm-4'>
        <ul class='list-unstyled social'>
          <li>
            <a href='https://www.facebook.com/Mauri-DMC-417773631609673/' target="_blank">
              <i class='fa fa-facebook'></i>
            </a>
          </li>
          <li>
            <a href='https://hu.linkedin.com/in/mauri-dmc-dóra-farkas-5780baa3'  target="_blank">
              <i class='fa fa-linkedin'></i>
            </a>
          </li>
          <li>
            <a href='mailto:incoming@mauridmc.hu'>
              <i class='fa fa-envelope'></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

