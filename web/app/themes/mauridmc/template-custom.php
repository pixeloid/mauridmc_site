<?php
/**
 * Template Name: Homepage
 */
?>

<?php while (have_posts()) : the_post(); ?>

	<div class='header-illustration-home'>
	  <div class='text'>
	    <h1 class='wow fadeInUp animated' data-wow-delay='1.5s'>
	    	<?php echo types_render_field('illustration-text') ?>
	    </h1>
	  </div>
	  <div class='illustration'>
	    <img class='ill-1 wow fadeInLeft animated' data-wow-delay='0.5s' src='<?php echo types_render_field('illustration-layer-1', array('output'=>'raw')); ?>'>
	    <img class='ill-2 wow fadeInLeft animated' data-wow-delay='0.6s' src='<?php echo types_render_field('illustration-layer-2', array('output'=>'raw')); ?>'>
	    <img class='ill-3 wow fadeInRight animated' data-wow-delay='0.7s' src='<?php echo types_render_field('illustration-layer-3', array('output'=>'raw')); ?>'>
	    <img class='ill-4 wow fadeInRight animated' data-wow-delay='0.8s' src='<?php echo types_render_field('illustration-layer-4', array('output'=>'raw')); ?>'>
	  </div>
	</div>

<?php endwhile; ?>
