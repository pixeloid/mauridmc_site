<?php 


add_filter('post_gallery', 'swiper_gallery', 10, 2);

function swiper_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => '',
        'type' => 'swiper'
    ), $attr));


    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments) || !isset($attr['type'])) $attr['type'] = 'carousel';

    switch ($post->post_type) {
        case 'collection':
                // Here's your actual output, you may customize it to your need
                $result = '';
                $portrait = false;
                $outputArr = array();
                $outputArr['l'] = array();
                $outputArr['p'] = array();
                $i = 0;
                // Now you loop through each attachment
                foreach ($attachments as $id => $attachment) {

                    $i ++;
                    $class = $i % 2 == 0 ? '' : 'right';

                    $img_l = wp_get_attachment_image_src($id, 'gallery');
                    $img_o = wp_get_attachment_image_src($id, 'orig');
                    $meta = wp_get_attachment_metadata( $id);

                    if($img_l[1] < $img_l[2]){
                        $portrait = true;
                        $class .= ' portrait';
                    }



                    $output = '<div class="collection-item ' . $class . '"  >';
                    $output .= '  <div class="parallax" " data-parallax=\'{"y": -80}\'>';
                    $output .= '             <a data-gallery="multiimages" data-toggle="lightbox"  href="' . $img_o[0] . '">';
                    $output .= '    <img src="'.$img_l[0].'" />';
                    $output .= "  </a>";
                    $output .= "  </div>";
                      # code...
                    if ($attachment->post_content || $attachment->post_excerpt) {
                        $output .= '  <div class="text" data-parallax=\'{"y": -30}\'>';
                        $output .= "    <h3>{$attachment->post_excerpt}</h3>";
                        $output .= "    <p>{$attachment->post_content}</p>";
                        $output .= "  </div>";
                    }
                    $output .= "</div>";

                    if($portrait){
                        $outputArr['p'][] = $output;
                    }else{
                        $outputArr['l'][] = $output;
                    }



                }

                foreach ($outputArr['p'] as $o) {
                    $result .= $o;
                }
                foreach ($outputArr['l'] as $o) {
                    $result .= $o;
                }

                return $result;

            break;

            case 'reference':
                $output = '';
                $images = array();
                foreach ($attachments as $id => $attachment) {   
                    $img = wp_get_attachment_image_src($id, 'square');  
                    $img_o = wp_get_attachment_image_src($id, 'large');
               
                    $images[] = array(
                        'img' => $img,
                        'img_o' => $img_o
                    );

                }

                $indicators = '';
                $items = '';
                $i = 0;

                foreach ($images as $img) {
                  $i ++;
                  $class = ($i == 1) ? 'active' : '';
                  $indicators .= "    <li class='" . $class . "' data-slide-to=' " . ($i-1) ."' data-target='#carousel-".$id."'></li>";

                }
                $i = 0;

                foreach ($images as $img) {
                  $i ++;
                  $class = ($i == 1) ? 'active' : '';

                  $items .= "    <div class='item " . $class . "'>";
                  $items .= '      <a data-gallery="multiimages" data-toggle="lightbox" class="enlarge" href="' . $img['img_o'][0] . '">';
                  $items .= "         <img alt='' class='img-responsive' src=' " . $img['img'][0] . "'>";
                  $items .= "      </a>";
                  $items .= "    </div>";


                }


                $output .= "<div class='carousel slide' data-ride='carousel' id='carousel-" . $id . "'>";
                $output .= "  <ol class='carousel-indicators'>";
                $output .= $indicators;
                $output .= "  </ol>";
                $output .= "  <div class='carousel-inner' role='listbox'>";
                $output .= $items;
                $output .= "  </div>";
                $output .= "  <a class='left carousel-control' data-slide='prev' href='#carousel-" . $id . "' role='button'>";
                $output .= "    <span aria-hidden='true' class='icon-prev'></span>";
                $output .= "    <span class='sr-only'>Previous</span>";
                $output .= "  </a>";
                $output .= "  <a class='right carousel-control' data-slide='next' href='#carousel-" . $id . "' role='button'>";
                $output .= "    <span aria-hidden='true' class='icon-next'></span>";
                $output .= "    <span class='sr-only'>Next</span>";
                $output .= "  </a>";
                $output .= "  <span class='zoom'></span>";
                $output .= "</div>";

                break;
        
        default:
            # code...
            break;
    }


    return $output;
}

